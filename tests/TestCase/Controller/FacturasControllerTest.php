<?php
namespace App\Test\TestCase\Controller;

use App\Controller\FacturasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\FacturasController Test Case
 */
class FacturasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.facturas',
        'app.clcs',
        'app.folio_envios',
        'app.contratacions',
        'app.partidas',
        'app.transferencias',
        'app.proveedors',
        'app.vigencias',
        'app.factoraje',
        'app.lineacaptura',
        'app.monto_por_campanas',
        'app.testigos',
        'app.digitales_orden_inserciones',
        'app.digitales_orden_inserciones_facturas',
        'app.impresos_fecha_inserciones',
        'app.facturas_impresos_fecha_inserciones',
        'app.impresos_orden_inserciones',
        'app.facturas_impresos_orden_inserciones',
        'app.pedidos',
        'app.facturas_pedidos',
        'app.radio_pautas',
        'app.facturas_radio_pautas',
        'app.television_pautas',
        'app.facturas_television_pautas'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
