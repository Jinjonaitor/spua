<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LineacapturaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LineacapturaTable Test Case
 */
class LineacapturaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LineacapturaTable
     */
    public $Lineacaptura;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.lineacaptura',
        'app.facturas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Lineacaptura') ? [] : ['className' => 'App\Model\Table\LineacapturaTable'];
        $this->Lineacaptura = TableRegistry::get('Lineacaptura', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Lineacaptura);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
