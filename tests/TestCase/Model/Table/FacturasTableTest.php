<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FacturasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FacturasTable Test Case
 */
class FacturasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FacturasTable
     */
    public $Facturas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.facturas',
        'app.clcs',
        'app.folio_envios',
        'app.contratacions',
        'app.partidas',
        'app.transferencias',
        'app.proveedors',
        'app.vigencias',
        'app.factoraje',
        'app.lineacaptura',
        'app.monto_por_campanas',
        'app.testigos',
        'app.digitales_orden_inserciones',
        'app.digitales_orden_inserciones_facturas',
        'app.impresos_fecha_inserciones',
        'app.facturas_impresos_fecha_inserciones',
        'app.impresos_orden_inserciones',
        'app.facturas_impresos_orden_inserciones',
        'app.pedidos',
        'app.facturas_pedidos',
        'app.radio_pautas',
        'app.facturas_radio_pautas',
        'app.television_pautas',
        'app.facturas_television_pautas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Facturas') ? [] : ['className' => 'App\Model\Table\FacturasTable'];
        $this->Facturas = TableRegistry::get('Facturas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Facturas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test defaultConnectionName method
     *
     * @return void
     */
    public function testDefaultConnectionName()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
