<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Facturas Controller
 *
 * @property \App\Model\Table\FacturasTable $Facturas
 */
class FacturasController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Clcs', 'FolioEnvios', 'Contratacions', 'Partidas', 'Transferencias', 'Proveedors', 'Vigencias']
        ];
        $facturas = $this->paginate($this->Facturas);

        $this->set(compact('facturas'));
        $this->set('_serialize', ['facturas']);
    }

    /**
     * View method
     *
     * @param string|null $id Factura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $factura = $this->Facturas->get($id, [
            'contain' => ['Clcs', 'FolioEnvios', 'Contratacions', 'Partidas', 'Transferencias', 'Proveedors', 'Vigencias', 'DigitalesOrdenInserciones', 'ImpresosFechaInserciones', 'ImpresosOrdenInserciones', 'Pedidos', 'RadioPautas', 'TelevisionPautas', 'Factoraje', 'Lineacaptura', 'MontoPorCampanas', 'Testigos']
        ]);

        $this->set('factura', $factura);
        $this->set('_serialize', ['factura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $factura = $this->Facturas->newEntity();
        if ($this->request->is('post')) {
            $factura = $this->Facturas->patchEntity($factura, $this->request->data);
            if ($this->Facturas->save($factura)) {
                $this->Flash->success(__('The factura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The factura could not be saved. Please, try again.'));
            }
        }
        $clcs = $this->Facturas->Clcs->find('list', ['limit' => 200]);
        $folioEnvios = $this->Facturas->FolioEnvios->find('list', ['limit' => 200]);
        $contratacions = $this->Facturas->Contratacions->find('list', ['limit' => 200]);
        $partidas = $this->Facturas->Partidas->find('list', ['limit' => 200]);
        $transferencias = $this->Facturas->Transferencias->find('list', ['limit' => 200]);
        $proveedors = $this->Facturas->Proveedors->find('list', ['limit' => 200]);
        $vigencias = $this->Facturas->Vigencias->find('list', ['limit' => 200]);
        $digitalesOrdenInserciones = $this->Facturas->DigitalesOrdenInserciones->find('list', ['limit' => 200]);
        $impresosFechaInserciones = $this->Facturas->ImpresosFechaInserciones->find('list', ['limit' => 200]);
        $impresosOrdenInserciones = $this->Facturas->ImpresosOrdenInserciones->find('list', ['limit' => 200]);
        $pedidos = $this->Facturas->Pedidos->find('list', ['limit' => 200]);
        $radioPautas = $this->Facturas->RadioPautas->find('list', ['limit' => 200]);
        $televisionPautas = $this->Facturas->TelevisionPautas->find('list', ['limit' => 200]);
        $this->set(compact('factura', 'clcs', 'folioEnvios', 'contratacions', 'partidas', 'transferencias', 'proveedors', 'vigencias', 'digitalesOrdenInserciones', 'impresosFechaInserciones', 'impresosOrdenInserciones', 'pedidos', 'radioPautas', 'televisionPautas'));
        $this->set('_serialize', ['factura']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Factura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $factura = $this->Facturas->get($id, [
            'contain' => ['DigitalesOrdenInserciones', 'ImpresosFechaInserciones', 'ImpresosOrdenInserciones', 'Pedidos', 'RadioPautas', 'TelevisionPautas']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $factura = $this->Facturas->patchEntity($factura, $this->request->data);
            if ($this->Facturas->save($factura)) {
                $this->Flash->success(__('The factura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The factura could not be saved. Please, try again.'));
            }
        }
        $clcs = $this->Facturas->Clcs->find('list', ['limit' => 200]);
        $folioEnvios = $this->Facturas->FolioEnvios->find('list', ['limit' => 200]);
        $contratacions = $this->Facturas->Contratacions->find('list', ['limit' => 200]);
        $partidas = $this->Facturas->Partidas->find('list', ['limit' => 200]);
        $transferencias = $this->Facturas->Transferencias->find('list', ['limit' => 200]);
        $proveedors = $this->Facturas->Proveedors->find('list', ['limit' => 200]);
        $vigencias = $this->Facturas->Vigencias->find('list', ['limit' => 200]);
        $digitalesOrdenInserciones = $this->Facturas->DigitalesOrdenInserciones->find('list', ['limit' => 200]);
        $impresosFechaInserciones = $this->Facturas->ImpresosFechaInserciones->find('list', ['limit' => 200]);
        $impresosOrdenInserciones = $this->Facturas->ImpresosOrdenInserciones->find('list', ['limit' => 200]);
        $pedidos = $this->Facturas->Pedidos->find('list', ['limit' => 200]);
        $radioPautas = $this->Facturas->RadioPautas->find('list', ['limit' => 200]);
        $televisionPautas = $this->Facturas->TelevisionPautas->find('list', ['limit' => 200]);
        $this->set(compact('factura', 'clcs', 'folioEnvios', 'contratacions', 'partidas', 'transferencias', 'proveedors', 'vigencias', 'digitalesOrdenInserciones', 'impresosFechaInserciones', 'impresosOrdenInserciones', 'pedidos', 'radioPautas', 'televisionPautas'));
        $this->set('_serialize', ['factura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Factura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $factura = $this->Facturas->get($id);
        if ($this->Facturas->delete($factura)) {
            $this->Flash->success(__('The factura has been deleted.'));
        } else {
            $this->Flash->error(__('The factura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
