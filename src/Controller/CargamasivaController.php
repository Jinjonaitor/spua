<?php
namespace App\Controller;
use Cake\Datasource\ConnectionManager;
use App\Controller\AppController;

/**
 * Lineacaptura Controller
 *
 * @property \App\Model\Table\LineacapturaTable $Lineacaptura
 */
class CargamasivaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function index()
    {
        $connection = ConnectionManager::get('seccap');
        $facturas = $connection
          ->execute("SELECT f.id as fact_id,f.estado,f.numero as factura, f.monto as importe_factura,f.proveedor_id,p.nombre as proveedor,f.clc_id,contratacion_id,
              			concat(tm.nombre,' ',cob.nombre) as tipo_medio,tm.id as medio_id,tm.nombre as medio,c.folio as clc,lc.numero as folio_cap,
              			lc.fecha as fecha_cap,lc.importe as importe_cap
              			FROM facturas as f
              			join proveedores as p on proveedor_id=p.id
              			join contrataciones as con on contratacion_id=con.id
              			join contratos as co on contrato_id=co.id
              			join servicios as s on servicio_id=s.id
              			join coberturas as cob on cobertura_id=cob.id
              			join tipo_medios as tm on cob.tipo_medio_id=tm.id
              			left join clcs as c on clc_id=c.id
              			join lineacaptura as lc on f.id=factura_id
              			where f.vigencia_id=3 and contratacion_id is not null and (f.estado='validada_por_financieros' or f.estado='validada')
              			order by f.numero,tm.id,tipo_medio,cob.id")
          ->fetchAll('assoc');
        $this->set(compact('facturas'));
        $this->set('_serialize', ['facturas']);
    }

    /**
     * View method
     *
     * @param string|null $id Lineacaptura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view()
    {
        if ($this->request->is('post')) {
              $datos=$this->request->data['facturas'];
              $connection = ConnectionManager::get('seccap');
              $datos=implode(", ",$datos);
              $datosInt = array_map('intval', explode(',', $datos));
              $facturas = $connection
                ->execute("select substr(clave_presupuestal,1,1) as gpo_id,substr(clave_presupuestal,2,2) as ur_id,'001' as ue_id,
                          substr(clave_presupuestal,4,2) as prg_id,substr(clave_presupuestal,6,2) as spr_id,substr(clave_presupuestal,8,2) as py_id,
                          substr(clave_presupuestal,10,3) as oa_id,substr(clave_presupuestal,13,3) as pg_id,substr(clave_presupuestal,16,3) as pe_id,
                          substr(clave_presupuestal,19,1) as fte_id,substr(clave_presupuestal,20,1) as sft_id,substr(clave_presupuestal,21,1) as ori_id,
                          substr(clave_presupuestal,22,1) as sor_id,substr(clave_presupuestal,23,1) as cso_id,substr(clave_presupuestal,24,2) as cfi_id,
                          substr(clave_presupuestal,26,2) as cfi_anio,prov.rfc as rfc_beneficiario,case when consolidada=true then 'C' else 'D' end  as tcp_id,
                          f.numero as referencia, f.fecha as fecha_referencia,f.subtotal as importe,f.iva as importe_iva,f.monto as importe_neto,f.concepto
                          from facturas as f
                          join contrataciones as con on contratacion_id=con.id
                          join partidas as p on con.partida_id=p.id
                          join proveedores as prov on proveedor_id=prov.id
                          join lineacaptura as lc on f.id=lc.factura_id
                          where f.id in (".$datos.")")
                ->fetchAll('assoc');

                $retenciones = $connection
                  ->execute("select substr(clave_presupuestal,1,1) as ger_id,substr(clave_presupuestal,2,1) as cgp_id,substr(clave_presupuestal,3,1) as rub_id,
                            substr(clave_presupuestal,4,1) as cgu_id,substr(clave_presupuestal,5,1) as cgsc_id,
                            substr(clave_presupuestal,7,1) as cta_gpo_id,substr(clave_presupuestal,8,2) as cta_ur_id,
                            substr(clave_presupuestal,11,3) as cta_ue_id,substr(clave_presupuestal,15,3) as cgc_id,substr(clave_presupuestal,19,4) as cta_id,
                            p.nombre as nombre_cuenta,prov.rfc as rfc_beneficiario,case when consolidada=true then 'C' else 'D' end  as tcp_id,
                            f.numero as referencia, f.fecha as fecha_referencia,subtotal as importe,iva as importe_iva,f.monto as importe_neto,f.concepto,
                            lc.numero as linea,f.id,lc.fecha as fecha_linea ,lc.importe as importe_linea
                            from facturas as f
                            join contrataciones as con on contratacion_id=con.id
                            join partidas as p on con.partida_id=p.id
                            join proveedores as prov on proveedor_id=prov.id
                            join lineacaptura as lc on f.id=lc.factura_id
                            where f.id in ( ".$datos." )")
                  ->fetchAll('assoc');
              $this->set(compact('retenciones'));
              $this->set('_serialize', ['retenciones']);
              $this->set(compact('facturas'));
              $this->set('_serialize', ['facturas']);
        }else {
          return $this->redirect(array('controller' => 'Cargamasiva', 'action' => 'index'));
        }

    }


}
