<?php
namespace App\Controller;
use Cake\Datasource\ConnectionManager;
use App\Controller\AppController;

/**
 * Lineacaptura Controller
 *
 * @property \App\Model\Table\LineacapturaTable $Lineacaptura
 */
class LineacapturaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function index()
    {
        $connection = ConnectionManager::get('seccap');
        $lineacaptura = $connection
          ->execute("select lc.id as linea,lc.numero as lineacaptura,lc.fecha,lc.importe,p.nombre as proveedor,
              				concat(tm.nombre,'-',cob.nombre) as tipo_medio,tm.id as medio_id,tm.nombre as medio,f.numero as factura
              				from lineacaptura as lc
              				join facturas as f on factura_id=f.id
              				join proveedores as p on proveedor_id=p.id
              				join contrataciones as con on contratacion_id=con.id
              				join contratos as co on contrato_id=co.id
              				join servicios as s on servicio_id=s.id
              				join coberturas as cob on cobertura_id=cob.id
              				join tipo_medios as tm on cob.tipo_medio_id=tm.id
              				order by lc.fecha,tm.id,tipo_medio,cob.id")
          ->fetchAll('assoc');
        $this->set(compact('lineacaptura'));
        $this->set('_serialize', ['lineacaptura']);
    }

    /**
     * View method
     *
     * @param string|null $id Lineacaptura id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $lineacaptura = $this->Lineacaptura->get($id, [
            'contain' => ['Facturas']
        ]);

        $this->set('lineacaptura', $lineacaptura);
        $this->set('_serialize', ['lineacaptura']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $lineacaptura = $this->Lineacaptura->newEntity();
        if ($this->request->is('post')) {
            $lineacaptura = $this->Lineacaptura->patchEntity($lineacaptura, $this->request->data);
            if ($this->Lineacaptura->save($lineacaptura)) {
                $this->Flash->success(__('The lineacaptura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lineacaptura could not be saved. Please, try again.'));
            }
        }
        $connection = ConnectionManager::get('seccap');
        $facturas = $connection
          ->execute("SELECT f.id as fact_id,f.estado,f.numero as factura, f.monto as importe_factura,f.proveedor_id,p.nombre as proveedor,contratacion_id,
              			concat(tm.nombre,'-',cob.nombre) as tipo_medio,tm.id as medio_id,tm.nombre as medio,lc.numero as linea
              			FROM facturas as f
              			join proveedores as p on proveedor_id=p.id
              			join contrataciones as con on contratacion_id=con.id
              			join contratos as co on contrato_id=co.id
              			join servicios as s on servicio_id=s.id
              			join coberturas as cob on cobertura_id=cob.id
              			join tipo_medios as tm on cob.tipo_medio_id=tm.id
              			left join lineacaptura as lc on f.id=factura_id
              			where f.vigencia_id=3 and contratacion_id is not null and f.estado='capturada' and lc.numero is null and p.nombre!='GOBIERNO DEL ESTADO DE OAXACA '
              			order by f.numero,tm.id,tipo_medio,cob.id")
          ->fetchAll('assoc');
        $this->set(compact('facturas'));
        $this->set('_serialize', ['facturas']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Lineacaptura id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $lineacaptura = $this->Lineacaptura->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $lineacaptura = $this->Lineacaptura->patchEntity($lineacaptura, $this->request->data);
            if ($this->Lineacaptura->save($lineacaptura)) {
                $this->Flash->success(__('The lineacaptura has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The lineacaptura could not be saved. Please, try again.'));
            }
        }
        $facturas = $this->Lineacaptura->Facturas->find('list', ['limit' => 200]);
        $this->set(compact('lineacaptura', 'facturas'));
        $this->set('_serialize', ['lineacaptura']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Lineacaptura id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $lineacaptura = $this->Lineacaptura->get($id);
        if ($this->Lineacaptura->delete($lineacaptura)) {
            $this->Flash->success(__('The lineacaptura has been deleted.'));
        } else {
            $this->Flash->error(__('The lineacaptura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
