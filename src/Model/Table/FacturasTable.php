<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Facturas Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Clcs
 * @property \Cake\ORM\Association\BelongsTo $FolioEnvios
 * @property \Cake\ORM\Association\BelongsTo $Contratacions
 * @property \Cake\ORM\Association\BelongsTo $Partidas
 * @property \Cake\ORM\Association\BelongsTo $Transferencias
 * @property \Cake\ORM\Association\BelongsTo $Proveedors
 * @property \Cake\ORM\Association\BelongsTo $Vigencias
 * @property \Cake\ORM\Association\HasMany $Factoraje
 * @property \Cake\ORM\Association\HasMany $Lineacaptura
 * @property \Cake\ORM\Association\HasMany $MontoPorCampanas
 * @property \Cake\ORM\Association\HasMany $Testigos
 * @property \Cake\ORM\Association\BelongsToMany $DigitalesOrdenInserciones
 * @property \Cake\ORM\Association\BelongsToMany $ImpresosFechaInserciones
 * @property \Cake\ORM\Association\BelongsToMany $ImpresosOrdenInserciones
 * @property \Cake\ORM\Association\BelongsToMany $Pedidos
 * @property \Cake\ORM\Association\BelongsToMany $RadioPautas
 * @property \Cake\ORM\Association\BelongsToMany $TelevisionPautas
 *
 * @method \App\Model\Entity\Factura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Factura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Factura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Factura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Factura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Factura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Factura findOrCreate($search, callable $callback = null)
 */
class FacturasTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('facturas');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Clcs', [
            'foreignKey' => 'clc_id'
        ]);
        $this->belongsTo('FolioEnvios', [
            'foreignKey' => 'folio_envio_id'
        ]);
        $this->belongsTo('Contratacions', [
            'foreignKey' => 'contratacion_id'
        ]);
        $this->belongsTo('Partidas', [
            'foreignKey' => 'partida_id'
        ]);
        $this->belongsTo('Transferencias', [
            'foreignKey' => 'transferencia_id'
        ]);
        $this->belongsTo('Proveedors', [
            'foreignKey' => 'proveedor_id'
        ]);
        $this->belongsTo('Vigencias', [
            'foreignKey' => 'vigencia_id'
        ]);
        $this->hasMany('Factoraje', [
            'foreignKey' => 'factura_id'
        ]);
        $this->hasMany('Lineacaptura', [
            'foreignKey' => 'factura_id'
        ]);
        $this->hasMany('MontoPorCampanas', [
            'foreignKey' => 'factura_id'
        ]);
        $this->hasMany('Testigos', [
            'foreignKey' => 'factura_id'
        ]);
        $this->belongsToMany('DigitalesOrdenInserciones', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'digitales_orden_insercione_id',
            'joinTable' => 'digitales_orden_inserciones_facturas'
        ]);
        $this->belongsToMany('ImpresosFechaInserciones', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'impresos_fecha_insercione_id',
            'joinTable' => 'facturas_impresos_fecha_inserciones'
        ]);
        $this->belongsToMany('ImpresosOrdenInserciones', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'impresos_orden_insercione_id',
            'joinTable' => 'facturas_impresos_orden_inserciones'
        ]);
        $this->belongsToMany('Pedidos', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'pedido_id',
            'joinTable' => 'facturas_pedidos'
        ]);
        $this->belongsToMany('RadioPautas', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'radio_pauta_id',
            'joinTable' => 'facturas_radio_pautas'
        ]);
        $this->belongsToMany('TelevisionPautas', [
            'foreignKey' => 'factura_id',
            'targetForeignKey' => 'television_pauta_id',
            'joinTable' => 'facturas_television_pautas'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('estado');

        $validator
            ->allowEmpty('numero');

        $validator
            ->decimal('monto')
            ->allowEmpty('monto');

        $validator
            ->decimal('subtotal')
            ->allowEmpty('subtotal');

        $validator
            ->decimal('iva')
            ->allowEmpty('iva');

        $validator
            ->date('fecha')
            ->allowEmpty('fecha');

        $validator
            ->date('fecha_entrega')
            ->allowEmpty('fecha_entrega');

        $validator
            ->allowEmpty('archivo_pdf');

        $validator
            ->allowEmpty('archivo_xml');

        $validator
            ->allowEmpty('concepto');

        $validator
            ->dateTime('created_at')
            ->allowEmpty('created_at');

        $validator
            ->dateTime('updated_at')
            ->allowEmpty('updated_at');

        $validator
            ->allowEmpty('justificacion');

        $validator
            ->allowEmpty('numero_transferencia');

        $validator
            ->boolean('factoraje')
            ->allowEmpty('factoraje');

        $validator
            ->boolean('oficio')
            ->allowEmpty('oficio');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['clc_id'], 'Clcs'));
        $rules->add($rules->existsIn(['folio_envio_id'], 'FolioEnvios'));
        $rules->add($rules->existsIn(['contratacion_id'], 'Contratacions'));
        $rules->add($rules->existsIn(['partida_id'], 'Partidas'));
        $rules->add($rules->existsIn(['transferencia_id'], 'Transferencias'));
        $rules->add($rules->existsIn(['proveedor_id'], 'Proveedors'));
        $rules->add($rules->existsIn(['vigencia_id'], 'Vigencias'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'seccap';
    }
}
