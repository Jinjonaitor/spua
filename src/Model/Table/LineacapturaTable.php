<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Lineacaptura Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Facturas
 *
 * @method \App\Model\Entity\Lineacaptura get($primaryKey, $options = [])
 * @method \App\Model\Entity\Lineacaptura newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Lineacaptura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Lineacaptura|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Lineacaptura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Lineacaptura[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Lineacaptura findOrCreate($search, callable $callback = null)
 */
class LineacapturaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('lineacaptura');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Facturas', [
            'foreignKey' => 'factura_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('numero');

        $validator
            ->date('fecha')
            ->allowEmpty('fecha');

        $validator
            ->decimal('importe')
            ->allowEmpty('importe');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['factura_id'], 'Facturas'));

        return $rules;
    }

    /**
     * Returns the database connection name to use by default.
     *
     * @return string
     */
    public static function defaultConnectionName()
    {
        return 'seccap';
    }
}
