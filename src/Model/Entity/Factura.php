<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Factura Entity
 *
 * @property int $id
 * @property string $estado
 * @property string $numero
 * @property float $monto
 * @property float $subtotal
 * @property float $iva
 * @property \Cake\I18n\Time $fecha
 * @property \Cake\I18n\Time $fecha_entrega
 * @property string $archivo_pdf
 * @property string $archivo_xml
 * @property string $concepto
 * @property int $clc_id
 * @property int $folio_envio_id
 * @property int $contratacion_id
 * @property int $partida_id
 * @property int $transferencia_id
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $updated_at
 * @property string $justificacion
 * @property int $proveedor_id
 * @property string $numero_transferencia
 * @property int $vigencia_id
 * @property bool $oficio
 *
 * @property \App\Model\Entity\Factoraje[] $factoraje
 * @property \App\Model\Entity\Clc $clc
 * @property \App\Model\Entity\FolioEnvio $folio_envio
 * @property \App\Model\Entity\Contratacion $contratacion
 * @property \App\Model\Entity\Partida $partida
 * @property \App\Model\Entity\Transferencia $transferencia
 * @property \App\Model\Entity\Proveedor $proveedor
 * @property \App\Model\Entity\Vigencia $vigencia
 * @property \App\Model\Entity\Lineacaptura[] $lineacaptura
 * @property \App\Model\Entity\MontoPorCampana[] $monto_por_campanas
 * @property \App\Model\Entity\Testigo[] $testigos
 * @property \App\Model\Entity\DigitalesOrdenInsercione[] $digitales_orden_inserciones
 * @property \App\Model\Entity\ImpresosFechaInsercione[] $impresos_fecha_inserciones
 * @property \App\Model\Entity\ImpresosOrdenInsercione[] $impresos_orden_inserciones
 * @property \App\Model\Entity\Pedido[] $pedidos
 * @property \App\Model\Entity\RadioPauta[] $radio_pautas
 * @property \App\Model\Entity\TelevisionPauta[] $television_pautas
 */
class Factura extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
