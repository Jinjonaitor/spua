<div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"><strong>Carga Masiva</strong></h4>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                      Generación de Carga Masiva
                    </div>
                    <!-- /.panel-heading -->
                    <form class="" action="<?=$this->Url->build(['controller'=>'Cargamasiva','action'=>'view']);?>" method="post">

                    <div class="panel-body">
                        <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>SEL.</th>
                                    <th>CLC</th>
                                    <th>FACTURA</th>
                                    <th>IMPORTE</th>
                                    <th>LINEA DE CAPTURA</th>
                                    <th>FECHA</th>
                                    <th>IMPORTE</th>
                                    <th>PROVEEDOR</th>
                                    <th>TIPO DE MEDIO</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($facturas as $factura): ?>
                                <tr class="odd gradeX">
                                  <td class="text-center">
                                    <div class="radio">
                                          <label>
                                              <input type="checkbox" name="facturas[]" id="optionsRadios3" value="<?= $factura['fact_id'];?>">
                                          </label>
                                      </div>
                                  </td>
                                    <td><?= $factura['clc'];?></td>
                                    <td><?= $factura['factura'];?></td>
                                    <td>$ <?= $this->Number->format($factura['importe_factura']) ?></td>
                                    <td><?= $factura['folio_cap'];?></td>
                                    <td><?= $factura['fecha_cap'];?></td>
                                    <td>$ <?= $this->Number->format($factura['importe_cap']) ?></td>
                                    <td><?= $factura['proveedor'];?></td>
                                    <td><?= $factura['tipo_medio'];?></td>
                                </tr>
                              <?php endforeach; ?>

                            </tbody>
                        </table>
                        <!-- /.table-responsive -->
                        <div class="row">
                          <div class="col-md-12 text-right">
                            <button type="submit" class="btn btn-primary">Generar</button>
                            <a href="<?=$this->Url->build(['controller'=>'Index','action'=>'index']);?>"><span class="btn btn-danger"> Cancelar </span></a>
                          </div>
                        </div>
                    </div>
                    </form>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->


    </div>
    <?php $this->start('script');?>
    <script type="text/javascript">
      $(document).ready(function() {
          $('#dataTables-example').dataTable({
              "language": {"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"},
              "lengthChange": false,
              "paging": false,
              "dom": "<'row busqueda'<'col-sm-6'f><'col-sm-6'l>>"
          });
      });
    </script>
    <?php $this->end();?>
