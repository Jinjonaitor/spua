<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><strong>Agregar Usuario</strong></h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Información de usuario
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-8 col-md-offset-2">
                                    <form role="form" action="<?= $this->Url->build(['controller'=>'users','action'=>'add'])?>" method="post">
                                        <div class="form-group">
                                            <label>Usuario</label>
                                            <input type="text" class="form-control" placeholder="Usuario" id="" name="username" required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Contraseña</label>
                                            <input type="password" class="form-control" placeholder="Contraseña" id="" name="password" required="true">
                                        </div>
                                        <div class="form-group">
                                            <label>Rol</label>
                                            <select name="role" class="form-control">
                                                <option value="administrador">Administrador</option>
                                                <option value="impresos">Impresos</option>
                                                <option value="digitales">Digitales</option>
                                                <option value="radio">Radio</option>
                                                <option value="television">Televisión</option>
                                                <option value="financieros">Financieros</option>
                                            </select>
                                        </div>
                                        <div class="row">
                                          <div class="col-lg-12 text-right">
                                            <input type="submit" class="btn btn-primary" text="Guardar">
                                            <a href="<?= $this->Url->build(['controller'=>'users','action'=>'index'])?>"><label class="btn btn-danger">Cancelar</label></a>

                                          </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.col-lg-12 -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>



<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('role');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
-->
