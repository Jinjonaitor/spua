<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $lineacaptura->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $lineacaptura->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="lineacaptura form large-9 medium-8 columns content">
    <?= $this->Form->create($lineacaptura) ?>
    <fieldset>
        <legend><?= __('Edit Lineacaptura') ?></legend>
        <?php
            echo $this->Form->input('numero');
            echo $this->Form->input('fecha', ['empty' => true]);
            echo $this->Form->input('importe');
            echo $this->Form->input('factura_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
