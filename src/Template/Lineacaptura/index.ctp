<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header"><strong>Líneas de Captura</strong></h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
              <div class="col-lg-12">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          Líneas Capturadas
                          <a style="marging-right:20px;" href="<?= $this->Url->build(['controller'=>'lineacaptura','action'=>'add'])?>"><label class="btn btn-info">Agregar</label></a>
                      </div>
                      <!-- /.panel-heading -->
                      <div class="panel-body">
                          <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example" >
                              <thead>
                                  <tr>
                                      <th>Número</th>
                                      <th>Fecha</th>
                                      <th>Importe</th>
                                      <th>Factura</th>
                                      <th>Proveedor</th>
                                      <th>Tipo de Medio</th>
                                  </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($lineacaptura as $linea): ?>
                                  <tr class="odd gradeX">
                                      <td><?= $linea['lineacaptura'];?></td>
                                      <td><?= $linea['fecha'];?></td>
                                      <td>$ <?= $this->Number->format($linea['importe']) ?></td>
                                      <td><?= $linea['factura'];?></td>
                                      <td><?= $linea['proveedor'];?></td>
                                      <td><?= $linea['tipo_medio'];?></td>
                                  </tr>
                                <?php endforeach; ?>

                              </tbody>
                          </table>
                          <!-- /.table-responsive -->

                      </div>
                      <!-- /.panel-body -->
                  </div>
                  <!-- /.panel -->
              </div>
              <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->
        </div>
        <?php $this->start('script');?>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#dataTables-example').dataTable({
                    "language": {"url": "//cdn.datatables.net/plug-ins/1.10.12/i18n/Spanish.json"},
                    "dom": "<'row busqueda'<'col-sm-6'f><'col-sm-6'l>>" +
                    "<'row'<'col-sm-12'tr>>"+
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                    "lengthMenu": [[50, 100, 150, 200, -1], [50, 100, 150, 200, "Todos"]]
                });
            });
          </script>
        <?php $this->end();?>
