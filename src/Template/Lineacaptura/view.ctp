<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Lineacaptura'), ['action' => 'edit', $lineacaptura->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Lineacaptura'), ['action' => 'delete', $lineacaptura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lineacaptura->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lineacaptura'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="lineacaptura view large-9 medium-8 columns content">
    <h3><?= h($lineacaptura->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Numero') ?></th>
            <td><?= h($lineacaptura->numero) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($lineacaptura->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Importe') ?></th>
            <td><?= $this->Number->format($lineacaptura->importe) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Factura Id') ?></th>
            <td><?=$lineacaptura->factura->estado?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($lineacaptura->fecha) ?></td>
        </tr>
    </table>
</div>

<?=debug($lineacaptura)?>
