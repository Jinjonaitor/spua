<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="page-header"><strong>Líneas de Captura</strong></h4>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Registro de Línea de Captura
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                          <div class="col-md-12">
                            <form class="form-inline" method="post" action="<?= $this->Url->build(['controller'=>'lineacaptura','action'=>'add'])?>">
                              <!-- /.row -->
                               <div class="row">
                                 <div class="col-md-12 formRegistroLinea">
                                   <div class="form-group groupRegistroLinea">
                                     <label for="exampleInputName2">Folio:</label>
                                     <input type="number" class="form-control" id="exampleInputName2" name="numero">
                                   </div>
                                   <div class="form-group groupRegistroLinea">
                                     <label for="exampleInputEmail2">Fecha:</label>
                                     <input type="date" class="form-control" id="exampleInputEmail2" naem=fecha>
                                   </div>
                                   <div class="form-group groupRegistroLinea">
                                     <label for="exampleInputEmail2">Monto:</label>
                                     <input type="text" class="form-control" id="exampleInputEmail2" name="importe">
                                   </div>
                                 </div>
                               </div>
                               <!-- /.row  -->
                               <table width="100%" class="table table-striped table-bordered table-hover" >
                                   <thead>
                                       <tr>
                                           <th>SEL.</th>
                                           <th>FACTURA</th>
                                           <th>IMPORTE</th>
                                           <th>PROVEEDOR</th>
                                           <th>TIPO DE MEDIO</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                     <?php foreach ($facturas as $factura): ?>
                                       <tr class="odd gradeX">
                                         <td class="text-center">
                                           <div class="radio">
                                                 <label>
                                                     <input type="radio" name="factura_id" id="optionsRadios3" value="<?=$factura['fact_id'];?>">
                                                 </label>
                                             </div>
                                         </td>
                                         <td><?=$factura['factura']?></td>
                                         <td><?=$factura['importe_factura']?></td>
                                         <td><?=$factura['proveedor']?></td>
                                         <td><?=$factura['tipo_medio']?></td>
                                       </tr>
                                     <?php endforeach; ?>
                                   </tbody>
                               </table>
                               <!-- /.table-responsive -->
                               <button type="submit" class="btn btn-info">Generar</button>
                               <a href="<?= $this->Url->build(['controller'=>'lineacaptura','action'=>'index'])?>"><label class="btn btn-danger">Cancelar</label></a>
                             </form>

                          </div>

                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->


            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="lineacaptura form large-9 medium-8 columns content">
    <?= $this->Form->create($lineacaptura) ?>
    <fieldset>
        <legend><?= __('Add Lineacaptura') ?></legend>
        <?php
            echo $this->Form->input('numero');
            echo $this->Form->input('fecha', ['empty' => true]);
            echo $this->Form->input('importe');
            echo $this->Form->input('factura_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>-->
