<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Spua</title>

    <!-- Bootstrap Core CSS -->
    <?= $this->Html->css('bootstrap.min.css') ?>

    <!-- MetisMenu CSS -->
    <?= $this->Html->css('metisMenu.min.css') ?>

    <!-- Custom CSS -->
    <?= $this->Html->css('sb-admin-2.css') ?>

    <!-- Morris Charts CSS -->
    <?= $this->Html->css('morris.css') ?>

    <!-- Custom CSS -->
    <?= $this->Html->css('custom.css') ?>

    <!-- Custom Fonts -->
    <?= $this->Html->css('font-awesome.min.css') ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Spua | Coordinación General de Comunicación Social</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i>Salir</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?= $this->Url->build(['controller'=>'index','action'=>'index'])?>"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="<?= $this->Url->build(['controller'=>'index','action'=>'index'])?>">Reportes SECCAP</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="<?= $this->Url->build(['controller'=>'cargamasiva','action'=>'index'])?>"><i class="fa fa-table fa-fw"></i> Carga Masiva</a>
                        </li>
                        <li>
                            <a href="<?= $this->Url->build(['controller'=>'lineacaptura','action'=>'index'])?>"><i class="fa fa-edit fa-fw"></i> Línea de Captura</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <?= $this->fetch('content') ?>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <?= $this->Html->script('jquery.min.js') ?>

    <!-- Bootstrap Core JavaScript -->
    <?= $this->Html->script('bootstrap.min.js') ?>

    <!-- Metis Menu Plugin JavaScript -->
    <?= $this->Html->script('metisMenu.min.js') ?>

    <!-- Morris Charts JavaScript -->
    <?= $this->Html->script('raphael.min.js') ?>
    <?= $this->Html->script('morris.min.js') ?>
    <?= $this->Html->script('morris-data.js') ?>

    <!-- DataTables JavaScript -->
    <?= $this->Html->script('jquery.dataTables.min.js') ?>
    <?= $this->Html->script('dataTables.bootstrap.min.js') ?>
    <?= $this->Html->script('dataTables.responsive.js') ?>

    <!-- Custom Theme JavaScript -->
    <?= $this->Html->script('sb-admin-2.js') ?>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
      <?= $this->fetch('script') ?>


</body>

</html>
