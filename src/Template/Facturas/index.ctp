<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Factura'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="facturas index large-9 medium-8 columns content">
    <h3><?= __('Facturas') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('estado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero') ?></th>
                <th scope="col"><?= $this->Paginator->sort('monto') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subtotal') ?></th>
                <th scope="col"><?= $this->Paginator->sort('iva') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_entrega') ?></th>
                <th scope="col"><?= $this->Paginator->sort('archivo_pdf') ?></th>
                <th scope="col"><?= $this->Paginator->sort('archivo_xml') ?></th>
                <th scope="col"><?= $this->Paginator->sort('clc_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('folio_envio_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contratacion_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('partida_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('transferencia_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('updated_at') ?></th>
                <th scope="col"><?= $this->Paginator->sort('proveedor_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('numero_transferencia') ?></th>
                <th scope="col"><?= $this->Paginator->sort('vigencia_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('factoraje') ?></th>
                <th scope="col"><?= $this->Paginator->sort('oficio') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($facturas as $factura): ?>
            <tr>
                <td><?= $this->Number->format($factura->id) ?></td>
                <td><?= h($factura->estado) ?></td>
                <td><?= h($factura->numero) ?></td>
                <td><?= $this->Number->format($factura->monto) ?></td>
                <td><?= $this->Number->format($factura->subtotal) ?></td>
                <td><?= $this->Number->format($factura->iva) ?></td>
                <td><?= h($factura->fecha) ?></td>
                <td><?= h($factura->fecha_entrega) ?></td>
                <td><?= h($factura->archivo_pdf) ?></td>
                <td><?= h($factura->archivo_xml) ?></td>
                <td><?= $this->Number->format($factura->clc_id) ?></td>
                <td><?= $this->Number->format($factura->folio_envio_id) ?></td>
                <td><?= $this->Number->format($factura->contratacion_id) ?></td>
                <td><?= $this->Number->format($factura->partida_id) ?></td>
                <td><?= $this->Number->format($factura->transferencia_id) ?></td>
                <td><?= h($factura->created_at) ?></td>
                <td><?= h($factura->updated_at) ?></td>
                <td><?= $this->Number->format($factura->proveedor_id) ?></td>
                <td><?= h($factura->numero_transferencia) ?></td>
                <td><?= $this->Number->format($factura->vigencia_id) ?></td>
                <td><?= h($factura->factoraje) ?></td>
                <td><?= h($factura->oficio) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $factura->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $factura->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $factura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $factura->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
