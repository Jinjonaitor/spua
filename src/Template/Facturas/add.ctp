<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Facturas'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="facturas form large-9 medium-8 columns content">
    <?= $this->Form->create($factura) ?>
    <fieldset>
        <legend><?= __('Add Factura') ?></legend>
        <?php
            echo $this->Form->input('estado');
            echo $this->Form->input('numero');
            echo $this->Form->input('monto');
            echo $this->Form->input('subtotal');
            echo $this->Form->input('iva');
            echo $this->Form->input('fecha', ['empty' => true]);
            echo $this->Form->input('fecha_entrega', ['empty' => true]);
            echo $this->Form->input('archivo_pdf');
            echo $this->Form->input('archivo_xml');
            echo $this->Form->input('concepto');
            echo $this->Form->input('clc_id');
            echo $this->Form->input('folio_envio_id');
            echo $this->Form->input('contratacion_id');
            echo $this->Form->input('partida_id');
            echo $this->Form->input('transferencia_id');
            echo $this->Form->input('created_at');
            echo $this->Form->input('updated_at');
            echo $this->Form->input('justificacion');
            echo $this->Form->input('proveedor_id');
            echo $this->Form->input('numero_transferencia');
            echo $this->Form->input('vigencia_id');
            echo $this->Form->input('factoraje');
            echo $this->Form->input('oficio');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
