<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Factura'), ['action' => 'edit', $factura->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Factura'), ['action' => 'delete', $factura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $factura->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Facturas'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Factura'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Lineacaptura'), ['controller' => 'Lineacaptura', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="facturas view large-9 medium-8 columns content">
    <h3><?= h($factura->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Estado') ?></th>
            <td><?= h($factura->estado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero') ?></th>
            <td><?= h($factura->numero) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Archivo Pdf') ?></th>
            <td><?= h($factura->archivo_pdf) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Archivo Xml') ?></th>
            <td><?= h($factura->archivo_xml) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Numero Transferencia') ?></th>
            <td><?= h($factura->numero_transferencia) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($factura->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Monto') ?></th>
            <td><?= $this->Number->format($factura->monto) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subtotal') ?></th>
            <td><?= $this->Number->format($factura->subtotal) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Iva') ?></th>
            <td><?= $this->Number->format($factura->iva) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Clc Id') ?></th>
            <td><?= $this->Number->format($factura->clc_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Folio Envio Id') ?></th>
            <td><?= $this->Number->format($factura->folio_envio_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contratacion Id') ?></th>
            <td><?= $this->Number->format($factura->contratacion_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Partida Id') ?></th>
            <td><?= $this->Number->format($factura->partida_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Transferencia Id') ?></th>
            <td><?= $this->Number->format($factura->transferencia_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Proveedor Id') ?></th>
            <td><?= $this->Number->format($factura->proveedor_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Vigencia Id') ?></th>
            <td><?= $this->Number->format($factura->vigencia_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($factura->fecha) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha Entrega') ?></th>
            <td><?= h($factura->fecha_entrega) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($factura->created_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Updated At') ?></th>
            <td><?= h($factura->updated_at) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Factoraje') ?></th>
            <td><?= $factura->factoraje ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Oficio') ?></th>
            <td><?= $factura->oficio ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Concepto') ?></h4>
        <?= $this->Text->autoParagraph(h($factura->concepto)); ?>
    </div>
    <div class="row">
        <h4><?= __('Justificacion') ?></h4>
        <?= $this->Text->autoParagraph(h($factura->justificacion)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Lineacaptura') ?></h4>
        <?php if (!empty($factura->lineacaptura)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Numero') ?></th>
                <th scope="col"><?= __('Fecha') ?></th>
                <th scope="col"><?= __('Importe') ?></th>
                <th scope="col"><?= __('Factura Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($factura->lineacaptura as $lineacaptura): ?>
            <tr>
                <td><?= h($lineacaptura->id) ?></td>
                <td><?= h($lineacaptura->numero) ?></td>
                <td><?= h($lineacaptura->fecha) ?></td>
                <td><?= h($lineacaptura->importe) ?></td>
                <td><?= h($lineacaptura->factura_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Lineacaptura', 'action' => 'view', $lineacaptura->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Lineacaptura', 'action' => 'edit', $lineacaptura->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Lineacaptura', 'action' => 'delete', $lineacaptura->id], ['confirm' => __('Are you sure you want to delete # {0}?', $lineacaptura->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
